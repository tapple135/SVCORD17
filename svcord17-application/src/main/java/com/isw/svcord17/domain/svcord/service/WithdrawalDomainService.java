package com.isw.svcord17.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord17.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord17.sdk.domain.svcord.entity.CustomerReference;
import com.isw.svcord17.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord17.sdk.domain.svcord.entity.Svcord17;
import com.isw.svcord17.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord17.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord17.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord17.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord17.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord17.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord17.sdk.integration.partylife.entity.RetrieveLoginInputEntity;
import com.isw.svcord17.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord17.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord17.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord17.domain.svcord.command.Svcord17Command;
import com.isw.svcord17.integration.partylife.service.RetrieveLogin;
import com.isw.svcord17.integration.paymord.service.PaymentOrder;
import com.isw.svcord17.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord17.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord17Command svcord17Command;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder, Repository repo) {

    super(entityBuilder, repo);

  }

  @NewSpan
  @Override
  public com.isw.svcord17.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(
      com.isw.svcord17.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput) {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic

    RetrieveLoginInput retrieveLoginInput = integrationEntityBuilder.getPartylife().getRetrieveLoginInput()
        .setId("test1").build();
    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveLoginInput);

    if(!retrieveLoginOutput.getResult().equals("SUCCESS")){
      return null;
    }

    CustomerReferenceEntity customerReferenceEntity = new CustomerReferenceEntity();
    customerReferenceEntity.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReferenceEntity.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createServicingOrderProducerInput = this.entityBuilder
    .getSvcord()
    .getCreateServicingOrderProducerInput()
    .setCustomerReference(customerReferenceEntity)
    // .setProcessEndDate(null)
    // .setProcessStartDate(null)
    // .setServicingOrderType(null)
    // .setServicingOrderWorkDescription(null)
    // .setServicingOrderWorkProduct(null)
    // .setServicingOrderWorkResult(null)
    // .setThirdPartyReference(null)
    .build();

    Svcord17 svcord17 = svcord17Command.createServicingOrderProducer(createServicingOrderProducerInput);
    log.info("svcord17Command.createServicingOrderProduce svcord17.getId() : "+svcord17.getId());

    //payment order call
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD17");
    paymentOrderInput.setExternalSerive("SVCORD17");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(svcord17.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));
  
    svcord17Command.updateServicingOrderProducer(svcord17, updateServicingOrderProducerInput);
  
    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();
  
    return withdrawalDomainServiceOutput;

  }

}
